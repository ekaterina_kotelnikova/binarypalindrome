package ru.kev.binaryPalindrome;

import java.util.Scanner;

/**
 * Этот класс позволяет перевести число в двоичную систему и опеределить, является ли двоичное число палиндромом.
 *
 * @author Kotelnikova E.V. group 15oit20
 */

public class BinaryPalindrome {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        int number = scanner.nextInt();

        String binaryNumber = Integer.toBinaryString(number);

        System.out.print(binaryNumber);

        if (isPalindrome(binaryNumber)) {
            System.out.println(" - является палиндромом");
        } else {
            System.out.println(" - не является палиндромом");
        }
        ;
    }

    /**
     * Метод для определения, является ли строка палиндромом
     *
     * @param binaryNumber двоичное число
     * @return true, если число является палиндромом, иначе false
     */
    public static boolean isPalindrome(String binaryNumber) {
        for (int i = 0; i <= binaryNumber.length() / 2; i++) {
            if (binaryNumber.charAt(i) != binaryNumber.charAt(binaryNumber.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }
}
